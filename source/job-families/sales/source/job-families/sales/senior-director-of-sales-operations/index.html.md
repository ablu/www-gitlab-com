---
layout: job_page
title: "Senior Director of Sales Operations"
---

We have an exciting opportunity for a Global Sales Operations leader to join a growing software DevOps organization. Reporting to and partnering with the CRO, you will be responsible for creating, managing and executing strategic programs to drive key revenue, margin, and growth opportunities world wide.

## Responsibilities

* Partner with the Chief Revenue Officer on all non-customer facing issues impacting the success of the sales team.
* Manage the global sales planning process, including regular updates to territory and quotas, headcount and sales compensation plans.  
* Partner with the CRO, finance and HR to design, document, implement and monitor sales compensation plans.
* Collaborate with regional sales leaders and teams to develop and execute sales management disciplines and processes (territory assignment & reviews, weekly/quarterly forecast, QBRs, pipeline analysis and development, account planning, account assignments, quota/budget allocation).
* Provide pricing and contract support; manage approval process (deal sign off) between sales and finance.
* Lead, manage and evaluate the sales tools, processes, policies and programs to ensure continuous productivity and effectiveness.
* Manage and maintain the Sales Handbook
* Create Board level and other presentations for the Chief Revenue Officer

## Requirements

* 10-15 years of experience in a global SAAS sales or sales operations environment.
* Demonstrated passion for information and business intelligence; thorough understanding of sales processes and methodologies.
* Minimum of 5 years experience in Salesforce.com. 
* Experience growing within a small start-up. Strong ability to interact and influence effectively with all C-level executives and team members.
* Ability to work independently with a high degree of accountability, while also able to collaborate cross-functionally (finance, marketing, sales enablement, etc) with exceptional intrapersonal skills. 
* Exceptional written/verbal communication and presentation skills.
* Analytical and detail oriented, strong project management skills with a drive for results.
* Proven ability to thrive in a fluid, fast-paced, unpredictable environment.
* Unquestionable ethics, integrity and business judgment; you share our values, and work in accordance with those values.
