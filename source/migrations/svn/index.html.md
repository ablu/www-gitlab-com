---
layout: markdown_page
title: Migrate from SVN to GitLab
---

GitLab has no built-in tool to migrate from SVN, but we recommend checking out
[this excellent guide by Aboullaite Mohammed](https://aboullaite.me/migration-from-svn-to-gitlab/).