---
layout: markdown_page
title: "Manage Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Manage Team
{: #manage}

The Manage team works on the backend part of GitLab for the [Manage product
category]. Among other things, this means working on GitLab's functionality around
user, group and project administration, authentication, access control, and subscriptions.

[Manage product category]: /handbook/product/categories/#manage