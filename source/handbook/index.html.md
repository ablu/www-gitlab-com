---
layout: markdown_page
title: Team Handbook
twitter_image: '/images/tweets/handbook-gitlab.png'
---

The GitLab team handbook is the central repository for how we run the company. Printed it consists of over [1,000 pages of text](/handbook/tools-and-tips/#count-handbook-pages). As part of our value of being transparent the handbook is <a href="https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook">open to the world</a>, and we welcome feedback<a name="feedback"></a>. Please make a <a href="https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests">merge request</a> to suggest improvements or add clarifications.
Please use <a href="https://gitlab.com/gitlab-com/www-gitlab-com/issues">issues</a> to ask questions.

* [General](/handbook)
  * [Values](/handbook/values)
  * [General Guidelines](/handbook/general-guidelines)
  * [Handbook Usage](/handbook/handbook-usage)
  * [Communication](/handbook/communication)
  * [Security](/handbook/security)
  * [Anti-Harassment Policy](/handbook/anti-harassment)
  * [Signing legal documents](/handbook/signing-legal-documents)
  * [Tools and tips](/handbook/tools-and-tips)
  * [Leadership](/handbook/leadership)
  * [Secret Snowflake](/handbook/secret-snowflake)
  * [Using Git to update this website](/handbook/git-page-update)
* [People Operations](/handbook/people-operations)
  * [Benefits](/handbook/benefits)
  * [Code of Conduct](/handbook/people-operations/code-of-conduct/)
  * [Spending Company Money](/handbook/spending-company-money)
  * [Travel](/handbook/travel)
  * [Paid time off](/handbook/paid-time-off)
  * [Incentives](/handbook/incentives)
  * [Onboarding](/handbook/general-onboarding)
  * [Hiring](/handbook/hiring)
  * [Offboarding](/handbook/offboarding)
  * [Visas](/handbook/people-operations/visas/)
* [Engineering Function](/handbook/engineering/)
  * [Dev Backend Department](/handbook/engineering/dev-backend/)
    * [Create Team](/handbook/engineering/dev-backend/create/)
    * [Distribution Team](/handbook/engineering/dev-backend/distribution/)
    * [Geo Team](/handbook/engineering/dev-backend/geo/)
    * [Gitaly Team](/handbook/engineering/dev-backend/gitaly/)
    * [Gitter Team](/handbook/engineering/dev-backend/gitter/)
    * [Manage Team](/handbook/engineering/dev-backend/manage/)
    * [Plan Team](/handbook/engineering/dev-backend/plan/)
  * [Frontend Department](/handbook/engineering/frontend/)
  * [Infrastructure Department](/handbook/engineering/infrastructure/)
    * [Database Team](/handbook/engineering/infrastructure/database/)
    * [Production Team](/handbook/engineering/infrastructure/production/)
  * [Ops Backend Department](/handbook/engineering/ops-backend/)
    * [CI/CD Team](/handbook/engineering/ops-backend/ci-cd/)
    * [Configure Team](/handbook/engineering/ops-backend/configure/)
    * [Monitoring Team](/handbook/engineering/ops-backend/monitoring/)
    * [Secure Team](/handbook/engineering/ops-backend/secure/)
  * [Quality Department](/handbook/engineering/quality/)
  * [Security Department](/handbook/engineering/security/)
  * [Support Department](/handbook/support/)
  * [UX Department](/handbook/engineering/ux/)
* [Marketing](/handbook/marketing)
  * [Website](/handbook/marketing/website/)
  * [Blog](/handbook/marketing/blog)
  * [Social Media Guidelines](/handbook/marketing/social-media-guidelines)
  * [Marketing and Sales Development](/handbook/marketing/marketing-sales-development/)
    * [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
    * [Content Marketing](/handbook/marketing/marketing-sales-development/content/)
    * [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/)
    * [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)
    * [Online Marketing](/handbook/marketing/marketing-sales-development/online-marketing/)
  * [Corporate Marketing](/handbook/marketing/corporate-marketing/)
  * [Community Marketing](/handbook/marketing/community-marketing)
  * [Product Marketing](/handbook/marketing/product-marketing/)
    * [Demos](/handbook/marketing/product-marketing/demo/)
  * [Marketing Career Development](/handbook/marketing/career-development/)
* [Sales](/handbook/sales)
  * [Account Management](/handbook/account-management)
  * [Customer Success](/handbook/customer-success/)
  * [Reseller Channels](/handbook/resellers/)
  * Sales Operations - moved to [Business Operations](/handbook/business-ops)
  * [Business Operations](/handbook/business-ops)
  * [Reporting](/handbook/business-ops/reporting)
* [Finance](/handbook/finance)
  * [Stock Options](/handbook/stock-options)
  * [Board meetings](/handbook/board-meetings)
* [Product](/handbook/product)
  * [Release posts](/handbook/marketing/blog/release-posts/)
  * [Live streaming](/handbook/product/live-streaming)
  * [Making Gifs](/handbook/product/making-gifs)
  * [Data analysis](/handbook/product/data-analysis)
  * [Technical Writing](/handbook/product/technical-writing/)
  * [Markdown Guide](/handbook/product/technical-writing/markdown-guide/)
* [Legal](/handbook/legal)
  * [DMCA](/handbook/dmca/)

<style>
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
</style>
